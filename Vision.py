import numpy as np
import cv2
import time
import os

w = 480
h = 320
scaleX = 0.25 #used for resizing later
scaleY = 0.25 #used for resizing later

last_known_x = -1
last_known_y = -1


#classify the object's hue we are going to track
lower_red = np.array([160,20,20])
upper_red = np.array([180,255,255])

middle_of_image =((w*scaleX)/2)

#load up the camera drivers
os.system('sudo modprobe bcm2835-v4l2')

camera = cv2.VideoCapture(0)
camera.set(3,w)
camera.set(4,h) #set the camera parameters for height and width

#time.sleep(0.5) #let camera start up
dilatekernel = np.ones((3,3),np.uint8)
erodekernel = np.ones((3,3), np.uint8)

def TrackObj():
    start = time.time()
    success, image = camera.read()

    #if failed to capture image, print warning and exit
    if not success:
        print 'camera capture failed, exiting!'
        return -1,-1,-1,-1

    image = cv2.resize(image,None, fx= 0.5, fy= 0.5, interpolation = cv2.INTER_AREA)
    image = cv2.flip(image,-1)

    image_HSV = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    #lets have a look at the HSV picture first
    red_hue_mask = cv2.inRange(image_HSV, lower_red, upper_red)

    #use the Open combination image morphology to reduce small noisey
    red_hue_mask = cv2.morphologyEx(red_hue_mask, cv2.MORPH_OPEN, dilatekernel)

    # findContours returns a list of the outlines of the white shapes in the mask (and a hierarchy that we shall ignore)
    contours, hierarchy = cv2.findContours(red_hue_mask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    # If we have at least one contour, look through each one and pick the biggest
    if len(contours)>0:
        largest = 0
        area = 0
        for i in range(len(contours)):
            # get the area of the ith contour
            temp_area = cv2.contourArea(contours[i])
            # if it is the biggest we have seen, keep it
            if temp_area > area:
                area = temp_area
                largest = i
        # Compute the coordinates of the center of the largest contour
        coordinates = cv2.moments(contours[largest])
        #avoiding floating point divide by zero exceptions. can also
        #use a try catch
        if coordinates['m00'] > 0.0:
            target_x = int(coordinates['m10']/coordinates['m00'])
            target_y = int(coordinates['m01']/coordinates['m00'])

        else:
            target_x = 0
            target_y = 0

        # Pick a suitable diameter for our target (grows with the contour)
        diam = int(np.sqrt(area)/4)
        timetaken = time.time() - start
        global last_known_x
        global last_known_y
        x_rect,y_rect,w_boud,h_bound = cv2.boundingRect(contours[largest])

        last_known_x = target_x
        last_known_y = target_y
        return target_x, target_y, w_boud,h_bound,timetaken
    else:
        print 'no target detected'
        return -1,-1,-1,-1,-1
def Release():
    global camera
    camera.release()

