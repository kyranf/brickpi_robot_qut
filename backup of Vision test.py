import numpy as np
import cv2
import time
import os

w = 480
h = 320

#classify the object's hue we are going to track
lower_red = np.array([155,50,50])
upper_red = np.array([180,255,255])


#load up the camera drivers
os.system('sudo modprobe bcm2835-v4l2')

camera = cv2.VideoCapture(0)
camera.set(3,w)
camera.set(4,h) #set the camera parameters for height and width

#time.sleep(0.5) #let camera start up
dilatekernel = np.ones((3,3),np.uint8)
erodekernel = np.ones((3,3), np.uint8)


def TrackObj():
    start = time.time()
    success, image = camera.read()

    #if failed to capture image, print warning and exit
    if not success:
        print 'camera capture failed, exiting!'
        cv2.destroyAllWindows()
        exit()

    image = cv2.resize(image,None, fx= 0.5, fy= 0.5, interpolation = cv2.INTER_AREA)
    image = cv2.flip(image,-1)

    image_HSV = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    #lets have a look at the HSV picture first
    red_hue_mask = cv2.inRange(image_HSV, lower_red, upper_red)

    #now lets see it after the dilation to inflate what is left
    red_hue_mask = cv2.dilate(red_hue_mask, dilatekernel,iterations = 1)
    # findContours returns a list of the outlines of the white shapes in the mask (and a heirarchy that we shall ignore)
    contours, hierarchy = cv2.findContours(red_hue_mask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    # If we have at least one contour, look through each one and pick the biggest
    if len(contours)>0:
        largest = 0
        area = 0
        for i in range(len(contours)):
            # get the area of the ith contour
            temp_area = cv2.contourArea(contours[i])
            # if it is the biggest we have seen, keep it
            if temp_area > area:
                area = temp_area
                largest = i
        # Compute the coordinates of the center of the largest contour
        coordinates = cv2.moments(contours[largest])
        target_x = int(coordinates['m10']/coordinates['m00'])
        target_y = int(coordinates['m01']/coordinates['m00'])
        # Pick a suitable diameter for our target (grows with the contour)
        diam = int(np.sqrt(area)/4)
        # draw on a target
        cv2.circle(image,(target_x,target_y),diam,(0,255,0),1)
        cv2.line(image,(target_x-2*diam,target_y),(target_x+2*diam,target_y),(0,255,0),1)
        cv2.line(image,(target_x,target_y-2*diam),(target_x,target_y+2*diam),(0,255,0),1)
    cv2.imshow('View',image)

    key_pressed = cv2.waitKey(1)
    if key_pressed == 27:
        break

    time.sleep(0.01)


camera.release()
cv2.destroyAllWindows()
cv2.waitKey(10)
time.sleep(0.1)
cv2.waitKey(10)
cv2.waitKey(10)
exit()
