# This program is not fully tested to be working, but is a re-implementation of the digital encoder-based compass system for tracking your orientation 
# for odometry during run-time for the robot. The code was originally created in INB860 "Artificial Intelligence for Embedded Systems" unit for the
# line-following and map-traversing robot made with LEGO NXT kits. 

# The aim of the program is to fit the robot with a LEGO IR sensor, and to rotate in a 360 degree turn where the start and finish is marked by a rising/falling edge of a black or white line,
# depending how you set it up. The encoder values are recorded between these two events, and the difference in encoder values plus knowledge of the axle length between the wheels, and the wheel radius, 
# allows you to work out the effect that a difference in encoder values does to your robot's orientation (assuming no slip).

# the compass factor is saved to a text file on the operating system, and you can then load this up during the main robot program and initialize your compass subsystem/function loop.
# This worked great in INB860, and the same encoders/motors are being used here, so it should be good!
# - Kyran Findlater (And original code contributions from Ryan Steindl and Michael Richmond during INB860)






#the aim of this program is to use an IR light sensor to
#rotate 360 degrees, examine encoders, and work out the compass
#factor for continuous rotation adjustment of the robot pose Theta.

print 'starting compass calibration program'
print 'this will use a white background with black line as the 360 degree'
print 'turn indication, by scanning with an IR sensor from NXT Kit'
print 'the encoders from the NXT motors will be used for this too'

from BrickPi import *   #import BrickPi.py file to use BrickPi operations

BrickPiSetup()  # setup the serial port for communication

BrickPi.MotorEnable[PORT_A] = 1 #Enable the Motor A
BrickPi.MotorEnable[PORT_B] = 1 #Enable the Motor B
BrickPi.SensorType[PORT_2] = TYPE_SENSOR_LIGHT_ON
BrickPiSetupSensors()   #Send the properties of sensors to BrickPi

edge_triggers = 0
enc_right_end = 0
enc_right_start = 0

#max value of 1024, for ADC value of light (I think? could be 0-255 actually)
THRESH = 100
TIMEOUT  = 5
RIGHT = PORT_A
LEFT = PORT_B

#zero the encoders at start.
BrickPiUpdateValues()
BrickPi.EncoderOffset[RIGHT] = BrickPi.Encoder[RIGHT]
BrickPi.EncoderOffset[LEFT] = BrickPi.Encoder[LEFT]


#begin spinning slowly anticlockwise
BrickPi.MotorSpeed[RIGHT] = 10  #Set the speed of MotorA (-255 to 255)
BrickPi.MotorSpeed[LEFT] = -10  #Set the speed of MotorB (-255 to 255)

time = time.time()


#scan sensors while rotating until hit first black line, record encoder value
while True:
    BrickPiUpdateValues()
    #check if over a black line
    if BrickPi.Sensor[PORT_2] < THRESH:
        BrickPiUpdateValues()
        if BrickPi.Sensor[PORT_2] < THRESH:
            enc_right_start = BrickPi.Encoder[RIGHT]
            edge_triggers += 1
            break

    if (time.time() - time)  > TIMEOUT):
        print 'calibration timed out! finding first black line'
        exit()

time = time.time() #restart the timeout reference time
while True:
    BrickPiUpdateValues()
    if BrickPi.Sensor[PORT_2] > THRESH:
        #we have gone back over white, so get ready now
        #for another black crossing to signal end
        break
    if (time.time() - time)  > TIMEOUT:
        print 'calibration timed out! getting off the black line'
        exit()

time = time.time() #restart the timeout reference time
while True:
    BrickPiUpdateValues()
    #check if over a black line
    if(BrickPi.Sensor[PORT_2] < THRESH):
        BrickPiUpdateValues() #check again for debounce purposes
        if(BrickPi.Sensor[PORT_2] < THRESH):
            enc_right_end = BrickPi.Encoder[RIGHT]
            edge_triggers += 1
            BrickPi.MotorSpeed[RIGHT] = 0  #Set the speed of MotorA (-255 to 255)
            BrickPi.MotorSpeed[LEFT] = 0  #Set the speed of MotorB (-255 to 255)
            break

    if(time.time() - time  > TIMEOUT):
        print 'calibration timed out! getting off the black line'
        exit()

#now we have the two encoder values for a full 360 turn, find the compass
#factor and record it to .text file in the working directory.

compassFactor = float(180.0/(enc_right_end - enc_right_start))

try:
    # This will create a new file or **overwrite an existing file**.
    f = open("compass.txt", "w")
    try:
        stringrepresentation = "{:0.8f}".format(compassFactor)
        print stringrepresentation, ' is the compass factor!'
        f.write(stringrepresentation) # Write a string to a file

    finally:
        f.close()
except IOError:
    print 'failed to write compass calibration file!'
    exit()

print 'done calibrating compass, now you can run the other programs!'
time.sleep(1)
