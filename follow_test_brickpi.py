import numpy as np
import cv2
import time
import os
from BrickPi import *   #import BrickPi.py file to use BrickPi operations
import RPi.GPIO as GPIO
import random
import math

w = 480
h = 320
scaleX = 0.25 #used for resizing later
scaleY = 0.25 #used for resizing later

LED1 = 12
LED2 = 13
ON = 1
OFF = 0
RIGHT = PORT_A
LEFT = PORT_D
MAX_SPEED = 100
HORIZ_THRESH = 25
DIAM_CLOSE = 30
left_speed = 0
right_speed = 0

#setup LEDS
GPIO.setmode(GPIO.BOARD)
GPIO.setup(LED1, GPIO.OUT) #gpio18 on rpi
GPIO.setup(LED2, GPIO.OUT) #gpio27 on rpi

def LED1_state( STATE ):
    GPIO.output(LED1, STATE)

def LED2_state( STATE ):
    GPIO.output(LED2, STATE)

BrickPiSetup()

BrickPi.MotorEnable[RIGHT] = 1 #Enable the Motor A
BrickPi.MotorEnable[LEFT] = 1 #Enable the Motor B
BrickPiSetupSensors()   #Send the properties of sensors to BrickPi


lower_red = np.array([155,20,20])
upper_red = np.array([180,255,255])

#load up the camera drivers
os.system('sudo modprobe bcm2835-v4l2')

camera = cv2.VideoCapture(0)
camera.set(3,w)
camera.set(4,h) #set the camera parameters for height and width

time.sleep(0.5)
dilatekernel = np.ones((3,3),np.uint8)
erodekernel = np.ones((3,3), np.uint8)
start = time.time()
last_time = time.time()

while (True):

    start = time.time()
    success, image = camera.read()

    #if failed to capture image, print warning and exit
    if not success:
        print 'camera capture failed, exiting!'
        cv2.destroyAllWindows()
        exit()

    #resize the image by half, for faster compute
    image = cv2.resize(image,None, fx= scaleX, fy= scaleY, interpolation = cv2.INTER_AREA)
    image = cv2.flip(image,-1) #the way im testing, image needs flipping

    image_HSV = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    #lets have a look at the HSV picture first
    red_hue_mask = cv2.inRange(image_HSV, lower_red, upper_red)

    #now lets see it after the dilation to inflate what is left
    red_hue_mask = cv2.morphologyEx(red_hue_mask, cv2.MORPH_OPEN, dilatekernel)
    # findContours returns a list of the outlines of the white shapes in the mask (and a heirarchy that we shall ignore)
    contours, hierarchy = cv2.findContours(red_hue_mask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    # If we have at least one contour, look through each one and pick the biggest
    if len(contours)>0:
        largest = 0
        area = 0
        for i in range(len(contours)):
            # get the area of the ith contour
            temp_area = cv2.contourArea(contours[i])
            # if it is the biggest we have seen, keep it
            if temp_area > area:
                area = temp_area
                largest = i
        # Compute the coordinates of the center of the largest contour
        coordinates = cv2.moments(contours[largest])

        #avoiding floating point divide by zero exceptions. can also
        #use a try catch
        if coordinates['m00'] > 0.0:
            target_x = int(coordinates['m10']/coordinates['m00'])
            target_y = int(coordinates['m01']/coordinates['m00'])

        else:
            target_x = 0
            target_y = 0


        # Pick a suitable diameter for our target (grows with the contour)
        diam = int(np.sqrt(area/np.pi)*2)
        # calc time
        timetaken = time.time() - start
        fps = 1.0 / timetaken
        print target_x, target_y, timetaken, fps

        middle_of_image =((w*scaleX)/2)

        if target_x <= middle_of_image-10:
            #target is on left side of image
            LED1_state(OFF)
            LED2_state(ON)
            left_speed = -MAX_SPEED/2
            right_speed = MAX_SPEED

        else:
            if target_x >= middle_of_image+10:
                #target is on right side of image
                LED1_state(ON)
                LED2_state(OFF)
                left_speed = MAX_SPEED
                right_speed = -MAX_SPEED/2
            else:
                #must be in the middle of our range
                left_speed = MAX_SPEED
                right_speed = MAX_SPEED
                LED1_state(ON)
                LED2_state(ON)

            #check if the ball is really close/large, to stop/slow down
        if diam > DIAM_CLOSE:
            left_speed = left_speed - 20
            right_speed = right_speed -20
            #clamp speeds to 0
            if left_speed < 0:
                left_speed = 0
            if right_speed < 0:
                right_speed = 0

    else:
        #no target was detected
        LED1_state(OFF)
        LED2_state(OFF)
        left_speed = 0
        right_speed= 0



    BrickPi.MotorSpeed[LEFT] = left_speed*-1
    BrickPi.MotorSpeed[RIGHT] = right_speed*-1
    BrickPiUpdateValues()


#end of while True loop

camera.release()
cv2.destroyAllWindows()
cv2.waitKey(10)
time.sleep(0.1)
cv2.waitKey(10)
cv2.waitKey(10)
exit()
