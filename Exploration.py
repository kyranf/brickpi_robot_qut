'''
Created on 18/06/2014

@author: Kyran Findlater
'''


#using enum34 module which has backports for 2.7 to make
#enums for use as state machine handling
from enum import Enum
from BrickPi import *   #import BrickPi.py file to use BrickPi operations
import RPi.GPIO as GPIO
import time
import Vision
import random
import math
import sys
import signal

import numpy as np


#if this is 1, debug print statements will be produced at regular points
DEBUG = 1


LED1 = 12
LED2 = 13
ON = 1
OFF = 0

STOP_BUTTON = PORT_1
RIGHT = PORT_A
LEFT = PORT_B
SONAR = PORT_4
MAX_SPEED = 150

#declare some timeout values, in seconds
AVOID_TIMEBASE = 0.5
avoid_randomtime = 0.0 #will be AVOID_TIMEBASE + RandomTime() later.
TURN_TIMEBASE = 0.5
turn_randomtime = 0.0 # will be TURN_TIMEBASE + RandomTime() later.
WALK_TIMEBASE = 3.0
walk_randomtime = 0.0 #will be WALK_TIMEBASE + RandomTime() later.

RAMP_INCREMENT = MAX_SPEED / 10  #gives incremental speed increase/decrease
SEARCH_TIME = 3.0
RANDOM_TIME_BOUNDS = 3.0
TURN_TIME = 2
AVOID_DIST = 40
PI = np.pi
STOP_RANGE = 35 #sonar value reading threshold for obstacle avoidance

WHEELRADIUS = 26 #mm
compassFactor = 0.0
axle_length = 0.0

ButtonVal = 0
SonarVal = 0
Last_Left_Enc = 0
Last_Right_Enc = 0

odom_x = 0.0 #meters
odom_y = 0.0 #meters
theta  = 0.0 #degrees
compass = 0.0 #degrees

#state labels
class Mission_State(Enum):
    Explore = 1
    Avoid = 2
    Follow = 3
    Found = 4
    Grab = 5
    Turning = 6
    Pause = 7

class Target:
    target_x = 0.0
    target_y = 0.0
    target_bound_w = 0.0
    target_bound_h = 0.0
    def __init__(self, x, y, w, h):
        self.target_x = x
        self.target_y = y
        self.target_bound_w = w
        self.target_bound_h = h

    def GetX(self):
        return self.target_x
    def GetY(self):
        return self.target_y
    def GetBoxWidth(self):
        return self.target_bound_w
    def GetBoxHeight(self):
        return self.target_bound_h
    def GetBoundArea(self):
        width = self.GetBoxWidth()
        height = self.GetBoxHeight()
        return (width * height)


#make some global class objects
LastTarget = Target(0,0,0,0)
Current_State = Mission_State.Explore
Last_State = Current_State #remember last state transition
if __name__ == '__main__':
    pass
else:
    quit()

#setup LEDS
GPIO.setmode(GPIO.BOARD)
GPIO.setup(LED1, GPIO.OUT) #gpio18 on rpi
GPIO.setup(LED2, GPIO.OUT) #gpio27 on rpi

def LED1_state( STATE ):
    GPIO.output(LED1, STATE)

def LED2_state( STATE ):
    GPIO.output(LED2, STATE)

def EndProgram():
    LED1_state(OFF)
    LED2_state(OFF)
    GPIO.cleanup()
    Vision.Release()
    #TODO add more cleanup and finishing stuff here
    sys.exit(0)

def ResetEncoders():
    #zero the encoders at start.
    BrickPiUpdateValues()
    BrickPi.EncoderOffset[RIGHT] = BrickPi.Encoder[RIGHT]
    BrickPi.EncoderOffset[LEFT] = BrickPi.Encoder[LEFT]

def RandomTime():
    return random.uniform(1, RANDOM_TIME_BOUNDS)

def constrain(x, upper, lower):
    if x > upper:
        return upper
    elif x < lower:
        return lower
    elif x > lower and x < upper:
        return x

#speed is 1 for forward, -1 for backwards, and 0 for halt.
def RampSpeed(motor, speed):
    global left_speed
    global right_speed
    global MAX_SPEED
    global RAMP_INCREMENT
    if motor == LEFT:
        #if speed is negative, check max and decrement
        if speed > 0:
            left_speed = constrain((left_speed + RAMP_INCREMENT), MAX_SPEED, -MAX_SPEED)

        #if speed is positive, check max and increment
        elif speed < 0:
            left_speed = constrain((left_speed - RAMP_INCREMENT), MAX_SPEED, -MAX_SPEED)

        #if speed is 0 "halt"
        elif speed == 0:
            if left_speed > 0:
                left_speed = left_speed - RAMP_INCREMENT
            elif left_speed < 0:
                left_speed = left_speed + RAMP_INCREMENT
            elif left_speed == 0:
                left_speed = 0

    elif motor == RIGHT:
        if speed > 0:
            right_speed = constrain((right_speed + RAMP_INCREMENT), MAX_SPEED, -MAX_SPEED)

        elif speed < 0:
            right_speed = constrain((right_speed - RAMP_INCREMENT), MAX_SPEED, -MAX_SPEED)


#simple function to quickly check just the button value and update
#global variable. Also returns result
def CheckButton():
    global ButtonVal
    BrickPiUpdateValues()
    ButtonVal = BrickPi.Sensor[STOP_BUTTON]
    return ButtonVal



def UpdateSensorsAndOdom():
    global SonarVal
    global Last_Left_Enc
    global Last_Right_Enc
    global ButtonVal
    BrickPiUpdateValues()

    ButtonVal = BrickPi.Sensor[STOP_BUTTON]

    SonarVal = BrickPi.Sensor[SONAR]
    if DEBUG == 1:
        print 'SonarVal: ', SonarVal

    Left_Enc = BrickPi.Encoder[LEFT]
    Right_Enc = BrickPi.Encoder[RIGHT]
    if DEBUG == 1:
        print 'LeftEnc: ', Left_Enc, ' RightEnc: ', Right_Enc
    UpdateOdom(Left_Enc, Right_Enc)

#uses global variable compassfactor, which is loaded from calib file
#gives you (change of) rotation based on scalar difference between left and right
#wheel encoders (change of)
def GetTheta(left_encoder, right_encoder):
    global compassFactor
    theta = compassFactor * (right_encoder - left_encoder)

    return float(theta)

def UpdateOdom(left_encoder, right_encoder):
    global odom_x
    global odom_y
    global compass
    global Last_Right_Enc
    global Last_Left_Enc
    global axel_length

    delta_left = left_encoder - Last_Left_Enc
    delta_right = right_encoder - Last_Right_Enc

    theta = GetTheta(delta_left, delta_right)
    deltaX = 0.0
    deltaY = 0.0
    if (delta_right - delta_left == 0):
        deltaX = ((delta_left*(PI/180.0)) * WHEELRADIUS) * (math.cos( math.radians(compass - 90.0 )))
        deltaY = ((-delta_left*(PI/180.0)) * WHEELRADIUS) * (math.sin( math.radians(compass - 90.0 )))
        compass += theta #update pose compass theta
        odom_x += deltaX #update pose X
        odom_y += deltaY #update pose Y

    else:
        pivot_length = (axle_length * delta_left)  /  (delta_right - delta_left);
        deltaX = (pivot_length + (axle_length / 2.0)) * (math.cos(math.radians(compass - 90.0 + theta)) + math.cos(math.radians(compass + 90.0)))
        deltaY = -(pivot_length + (axle_length / 2.0)) * (math.sin(math.radians(compass - 90.0 + theta)) + math.sin(math.radians(compass + 90.0)))
        compass += theta
        odom_x += deltaX
        odom_y += deltaY




def exit_signal_handler(signal, frame):
    print 'Control C or SIGINT detected, ending program!'
    EndProgram()

#attach signal handler for a cleaner exit from any generated SIGINT signals
#for example from a Kill command, or "Control+C" keyboard event.
signal.signal(signal.SIGINT, exit_signal_handler)

#continue with setup code
BrickPiSetup()  # setup the serial port for communication

BrickPi.MotorEnable[RIGHT] = 1 #Enable the Motor A
BrickPi.MotorEnable[LEFT] = 1 #Enable the Motor B
BrickPi.SensorType[SONAR] = TYPE_SENSOR_ULTRASONIC_CONT
BrickPi.SensorType[STOP_BUTTON] = TYPE_SENSOR_TOUCH
BrickPiSetupSensors()   #Send the properties of sensors to BrickPi

ResetEncoders()

#lets open the compass calibration file now and set up the digital compass
#to keep record of our "Theta" pose estimate.
try:
    f = open("compass.txt", "r")
    try:

        line = f.readline()
        compassFactor = float(line)
        #use this now to quickly calculate and initalize a key part for odometry
        axle_length = float((WHEELRADIUS / compassFactor));


    finally:
        f.close()
except IOError:
    print 'failed to open calibration file, the file may not exist yet'
    print 'try running the compass calib program first!'
    #DEBUG, IF YOU HAVENT DONE COMPASS YET
    compassFactor = 0.05
    #exit()

#initialize timers
avoidTimer = time.time()
turnTimer = time.time()
walkTimer = time.time()
followTimer = time.time()
timestarted = False
#overall state-checking while loop.
while True:

    #check state, and do whatever that state requires. Each state will have
    #it's own procedure. Some states will use certain sensors or not.

#========================== EXPLORE STATE ======================================
#in this state the robot attempts to visually find the target, otherwise does
#a random walk.
    if Current_State == Mission_State.Explore:

        #is there a target?
        targetx, targety, box_w, box_h, proc_time = Vision.TrackObj()

        if targetx <= 0:
            print 'no target detected this time'
        else:
            print 'target detected, attempting to follow!'

            #change state to follow, and make the state while loop
            #re-evaluate to enter the new state
            print 'entering follow state...'
            Current_State = Mission_State.Follow
            Last_State = Mission_State.Explore
            continue

        UpdateSensorsAndOdom()
        #check for the pause button
        if ButtonVal:
            time.sleep(0.1) #debounce

            if CheckButton():
                print 'button pressed, changing to pause state!'
                Last_State = Mission_State.Explore
                Current_State = Mission_State.Pause
                continue

        #collision avoidance routine/state change
        if SonarVal < STOP_RANGE:
            time.sleep(0.05)
            print 'obstacle may have been detected..'
            UpdateSensorsAndOdom()
            #check again as a form of de-bounce
            if SonarVal < STOP_RANGE:
                print 'obstacle detected! change to avoid state now...'
                #set last state, if we need to come back
                Last_State = Mission_State.Explore

                Current_State = Mission_State.Avoid
                continue

        #if we are still in this state, do some random movement using
        #timers and random offsets.
        walktimestart = False
        global walktimestart
        global walkTimer
        global walk_randomtime
        if walktimestart == False:
            if walk_randomtime == 0.0:
                walk_randomtime = WALK_TIMEBASE + RandomTime()
            walkTimer = time.time()
            print 'starting random walk with duration of ', walk_randomtime, ' seconds'
            walktimestart = True
            #TODO SET MOTORS TO FORWARD


        else:
            if time.time() - walkTimer >= walk_randomtime:
                #timeout, do a turn, and then do another walk later
                walkTimer = 0.0
                walk_randomtime = 0.0
                walktimestart = False
                print 'finished random walk, turning...'
                #TODO SET MOTORS TO STOP

                Last_state = Mission_State.Explore
                Current_state = Mission_State.Turning
                continue

        time.sleep(0.01)
        #end of Explore state

#========================== AVOID STATE =======================================
#avoid state is used when an obstacle is found in the path of the robot, and this
#will initiate the turn state after determining the direction and time to turn
    elif Current_State == Mission_State.Avoid:
        UpdateSensorsAndOdom()
        if SonarVal < STOP_RANGE:
            time.sleep(0.02)
            print 'obstacle may have been detected..'
            UpdateSensorsAndOdom()
            #keep turning if we still see obstacles
            if SonarVal < STOP_RANGE:
                avoidTimer = time.time()
                #avoidTimer resets, meaning turning will continue
                pass
        elif SonarVal >= STOP_RANGE:
            #check that the random time has been set yet
            if avoid_randomtime == 0.0:
                avoid_randomtime = AVOID_TIMEBASE + RandomTime()

            if time.time() - avoidTimer >= avoid_randomtime:
                #the path is clear for required time, so lets
                #get on with life and return to lastk nown state
                Current_State = Last_State
                Last_State = Mission_State.Avoid
                avoid_randomtime = 0.0
                continue


        if ButtonVal:
            time.sleep(0.1) #debounce

            if CheckButton():
                print 'button pressed, changing to pause state!'
                Last_State = Mission_State.Avoid
                Current_State = Mission_State.Pause
                continue

        time.sleep(0.01)
        #end of avoid state

#========================== FOLLOW STATE ======================================
#this state is the behaviour shown in the "follow test brickpi" program, where
#based on the camera returns the target will be followed and approached, where
#the robot can then enter the 'found' and 'grab' states.
    elif Current_State == Mission_State.Follow:

        #is there a target?
        targetx, targety, box_w, box_h, proc_time = Vision.TrackObj()

        if targetx <= 0:
            print 'no target detected this time'
            if timestarted:
                #print 'time since lost: ', (time.time() - followTimer)
                if time.time() - followTimer >= 1:
                    print 'target lost for too long, going to exploration state!'
                    Last_State = Mission_State.Follow
                    Current_State = Mission_State.Explore
                    followTimer = 0
                    timestarted = False
                    continue

            else:
                timestarted = True
                print 'starting target lost timeout timer'
                followTimer = time.time()


        else:
            print 'target detected at x: ', targetx, ' width is: ', box_w



        BrickPiUpdateValues()
        ButtonVal = BrickPi.Sensor[STOP_BUTTON]
        if ButtonVal:
            time.sleep(0.1) #debounce
            if CheckButton():
                print 'button pressed, changing to pause state!'
                Last_State = Mission_State.Follow
                Current_State = Mission_State.Pause
                continue



#========================== TURN STATE =======================================
#use this state to make the robot turn in a random direction for random time
#until the sonar gives clear readings (with a bit of extra time thrown in too)
    elif Current_State == Mission_State.Turning:



        if ButtonVal:
            time.sleep(0.1) #debounce
            if CheckButton():
                print 'button pressed, changing to pause state!'
                Last_State = Mission_State.Turning
                Current_State = Mission_State.Pause
                continue

        if turn_randomtime == 0.0:
            pass

#========================== FOUND STATE ======================================
#this state is for when the robot has found, and approached the red/coloured
#object and soon attempting to grab the object
    elif Current_State == Mission_State.Found:
        #unused
        pass
#========================== GRAB STATE ======================================
#this state is for when the robot is grabbing the object, and perhaps can use
#this state for returning to x = 0, y = 0 to deposit, or initiate a new state
# called 'return' or something.

    elif Current_State == Mission_State.Grab:
        #unused
        pass

#========================== PAUSE STATE ======================================
#used with the push button, to start/stop/pause the program. convenience state
#so you can approach and pick up the robot without it driving off etc.
    elif Current_State == Mission_State.Pause:

        #not checking any other sensors, so have to call check button first
        if CheckButton():
            time.sleep(0.1) #debounce
            if CheckButton():
                print 'button pressed, changing to last state : ', Last_State

                Current_State = Last_State
                continue
        pass


